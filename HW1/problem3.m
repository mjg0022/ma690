%problem 3
%kaczmarz algorithm
clc, clear all, format compact

%flag to cause the initial guess to be close to the actual answer
%this reduces run time
smallStartingDeviationFlag = 0;
displayProgressFlag = 0;

% Generate a random matrix
A = 100 * randn(25);

% Generate a matrix of all ones
b = ones(25, 1);

% Initialize the Kaczmarz algorithm
i         = 1;
iterCount = 0;
if smallStartingDeviationFlag
    %this will cause the initial guess
    xPrev     = A\b + .00001*rand(25,1);
else
    xPrev     = zeros(25, 1);
end

% generate the first guess in the algorithm
num = b(i) - dot(A(i,:)',xPrev);
den = norm(A(i,:))^2;
xNew  = xPrev + (num/den) * A(i,:)';

%update the guess until stopping criterion reached
while ( norm(diff([xPrev xNew],[],2)) > 1e-10 ) && ( iterCount <= 1e6 )
    
    iterCount = iterCount + 1;
    i         = mod(i, 25) + 1;
    
    if displayProgressFlag
        display([num2str(iterCount) char(9) num2str(norm(diff([xPrev xNew],[],2)))]);
    end
    
    % Update Kaczmarz Algorithm
    xPrev = xNew;
    
    num = b(i) - dot(A(i,:)',xPrev);
    den = norm(A(i,:))^2;
    xNew  = xPrev + (num/den) * A(i,:)';
    
end


%completion analysis
normOf_Ax_Minus_b = norm(A*xNew - b);


iterCount
kaczmarz__________matlab = [xNew A\b];
kaczmarz__________matlab
normOf_Ax_Minus_b
if max(diff(kaczmarz__________matlab,[],2)) < 1e-5
   display('kaczmarz answer is close to matlab answer');
else
   display('kaczmarz answer not close to matlab answer');
end





























