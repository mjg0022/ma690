%problem 2
%grid search for minimum
close all; clear all; clc

f = @(x,y) x^2 + y^2 + 25*(sin(x)^2 + sin(y)^2);

numLinPoints = 1001;

[xGrid, yGrid] = ndgrid(linspace(-10,10,numLinPoints));

values = arrayfun(f,xGrid, yGrid);

[minVal,minLoc] = min(values(:));
[minRow, minCol] = ind2sub(size(values),minLoc);
minXY = [xGrid(minRow,minCol) yGrid(minRow,minCol)];
%minVal

display(['numLinPoints: ' num2str(numLinPoints)]);
display(['minVal: ' num2str(minVal) ' found at ' mat2str(minXY,4)]);

f1 = figure;
mesh(xGrid,yGrid,values)
title('f = x^2 + y^2 + 25*(sin^2(x) + sin^2(y)')
xlabel('x'), ylabel('y'), zlabel('f')
colorbar;
a1 = gca;

%viewed from top
f2 = figure;
copyobj(a1,f2); colorbar
view(2)

%downfall of grid search methods:
%numLinPoints: 1000
%minVal: 0.0052102 found at [-0.01001 -0.01001]


