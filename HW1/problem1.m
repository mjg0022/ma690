%problem 1
%newton's method
clear all; clc

x0Vec = [-.3396, -.3386, -.3376, -.3366];
numIter = 100;

f = @(x) x^3 - 2*x^2 - 5*x + 6;
fDot = @(x) 3*x^2 - 4*x - 5;

x0VecSolutions = [];

for x0 = x0Vec
    solVec = nan(numIter,1); %;x0;
    solVec(1) = x0;
    for i = 1:numIter

        solVec(i+1) = solVec(i) - (f(solVec(i)) / fDot(solVec(i)));

    end
    
    x0VecSolutions(end+1,1) = solVec(end);
end


init_x0_______solution = [x0Vec' x0VecSolutions];
init_x0_______solution
