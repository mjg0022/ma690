%problem 1
%obtain the derivative of f
syms x

f = x^3 - 2*x^2 - 5*x + 6;

fDot = diff(f,x)