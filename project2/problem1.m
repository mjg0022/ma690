close all; clear all;

a = 1;
b = 3;

initialPoint = [1,1]; %starting point for gradient descent
initStepSize = .0001; %step size for gradient descent a_n+1 = a - stepsize*grad(f)

% function
f = @(x,y) x.^2 + y;

% gradient
fGradX = @(x,y) 2*x;
fGradY = @(x,y) 1;

fGrad = @(x,y) [2*x ; 1];


% theseX = xCon(-3:.1:3);
% theseY = yCon(-3:.1:3);
% theseF = f(theseX,theseY);

%plot 
[xGrid, yGrid] = ndgrid(linspace(-5,5,51));
values = arrayfun(f,xGrid, yGrid);
f1 = figure; hold on; grid on;
imagesc(xGrid(:,1),yGrid(1,:),values);
scatter(0,0,'g.');
scatter(initialPoint(1),initialPoint(2),'m.');
axis([min(xGrid(:,1)) max(xGrid(:,1)) min(yGrid(1,:)) max(yGrid(1,:))])
set(gca,'YDir','normal');

title({['initialPoint: ' mat2str(initialPoint)]}) % ['stepSize: ' num2str(initStepSize)]});
xlabel('x'); ylabel('y');
colorbar;

% constraint functions
% xCon = @(y) (-b*y)/a;
yCon = @(x) (-a*x)/b;
theseX = linspace(-5,5,51);
theseY = yCon(theseX);
scatter(theseX,theseY,'b');

%scatter(theseX,theseY);

% figure; hold on;
% scatter(theseX,theseF);

% %plot gradient
% [xGridLowRes, yGridLowRes] = ndgrid(linspace(-5,5,11));
% G1 = arrayfun(fGradX,xGridLowRes,yGridLowRes);
% G2 = arrayfun(fGradY,xGridLowRes,yGridLowRes);
% f2 = figure;
% quiver(xGridLowRes(:,1),yGridLowRes(1,:),G1,G2)
% title(' gradient'); xlabel('x'); ylabel('y');
% axis([min(xGrid(:,1)) max(xGrid(:,1)) min(yGrid(1,:)) max(yGrid(1,:))])

iter = 0;
alpha = initStepSize;
x = initialPoint.';
figure(f1);

A = [a ; b];

xNew = x;
xOld = 0*xNew + 100000;

while 1 < 2
    iter = iter + 1;
    %display(['iter: ' num2str(iter)])
       
    %gradient descent equation
    if iter == 1
        xNew = x - initStepSize*[fGrad(x(1),x(2))];
    else
        % this alpha equation isn't actually doing anything but the
        % gradient projection usually still works
        %barzilai borwein weights
        alpha = ((xNew - xOld).' * (fGrad(xNew(1),xNew(2))-fGrad(xOld(1),xOld(2)))) ...
            / (norm(fGrad(xNew(1),xNew(2)) - fGrad(xOld(1),xOld(2)))^2);
        
        x = xNew - alpha*fGrad(xNew(1),xNew(2));
        
        % project onto the line ax + by = 0        
        x = (eye(2) - ((A*A.')/(A.'*A)))*x;
        
        xOld = xNew;
        xNew = x;
    end
    
    scatter(x(1),x(2),'m.')
    drawnow
    
    %M(iter) = getframe;
    %norm(xNew-xOld)
    if iter > 1000 || ( norm(xNew-xOld) < 1e-10 )
        break
    end
    
    %pause(.05)
end


display(['iterations: ' num2str(iter)])
display(['final point: ' mat2str(x,5)])
%comet(a(:,1),a(:,2))

















