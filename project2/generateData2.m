% Generate Data
rng(312)
theta1 = rand(1, 250) * 2 * pi;
theta2 = rand(1, 250) * 2 * pi;
data1 = (8 + randn(250, 1)) .* [cos(theta1); sin(theta1)].';
data2 = 1*randn(250, 1) .* [cos(theta2); sin(theta2)].';

% Collect Data into one Matrix
data = [data1; data2];

% Generate class vector
c = ones(size(data, 1), 1);
c(size(data2, 1)+1:end) = -1;