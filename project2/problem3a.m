clear all; close all;

generateData2

figure; hold on;
%scatter(data1(:,1),data1(:,2));
%scatter(data2(:,1),data2(:,2));

phi = @(x,y) [x.^2, sqrt(2).*x.*y, y.^2];

transformedData  = phi(data(:,1),data(:,2));
transformedData1 = phi(data1(:,1),data1(:,2));
transformedData2 = phi(data2(:,1),data2(:,2));

%transformData = @(x) (1 + 1E-5*x).^2;

scatter3(transformedData1(:,1) , transformedData1(:,2) , transformedData1(:,3));
scatter3(transformedData2(:,1) , transformedData2(:,2) , transformedData2(:,3));


%legend('data1','data2');

X = transformedData;
lambdaBound = .1;

numData=size(X,1);
lambda=zeros(numData,1);
lambda2=zeros(numData,1);

% convergence
normW=Inf;
convergenceNorm=1e-10;

% Kernel
Kernel=X*X';
%genKernel = @(x) (1 + 1E-5*x).^2;
%Kernel = genKernel(Kernel);

iter=0;

% gradient descent
while normW > convergenceNorm
    
    iter=iter+1;
    lambdaOld=lambda;
    w1=(lambdaOld.*c).*X;
    
    for i=1:numData
             
        for j=1:numData
            lambda2(j,1)=lambda(j,1)*c(j,1)*Kernel(i,j);
        end
        
        lambda(i,1)= lambda(i,1)+(1/Kernel(i,i))*(1-(c(i,1)*sum(lambda2)));
    
        % set a bound on lambda to prevent divergence
        if lambda(i,1)<0
            lambda(i,1)=0;
        elseif lambda(i,1)>lambdaBound
            lambda(i,1)=lambdaBound; 
        end
        
    end
    
    w2=(lambda.*c).*X;
    normW=norm(w2-w1);
    
    if iter > 1000
       break 
    end
    
end

lamPos = lambda > 0;
X_lamPos=X(lamPos,:); 
Y_lamPos=c(lamPos>0);

W=(lambda(lamPos).*Y_lamPos)'*X_lamPos;

b=mean(Y_lamPos-(X_lamPos*W'));  

ft=X*W'+b;

[xGrid , yGrid] = meshgrid(linspace(-15,15,10));
surf(xGrid,yGrid, -(W(1) * xGrid + W(2) * yGrid) ./ W(3) + 20);

view(3);

legend('data1','data2','boundary')

W

% don't plot the line
% xPoints = [-50 50];
% boundary = (-b-W(1)*xPoints)/W(2);
% axis manual
% plot(xPoints,boundary,'LineWidth',2,'Color','g');
% legend('data1','data2','svm boundary');
% title('SVM Boundary')



