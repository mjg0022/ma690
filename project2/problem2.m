clear all; close all;

generateData1

figure; hold on;
scatter(data1(:,1),data1(:,2));
scatter(data2(:,1),data2(:,2));

legend('data1','data2');

X = data;
lambdaBound = .1;

numData=size(X,1);
lambda=zeros(numData,1);
lambda2=zeros(numData,1);

% convergence
normW=Inf;
convergenceNorm=1e-10;

% Kernel
Kernel=X*X'; 

total_iterations=0;

% gradient descent
while normW > convergenceNorm
    
    total_iterations=total_iterations+1;
    lambdaOld=lambda;
    w1=(lambdaOld.*c).*X;
    
    for i=1:numData
             
        for j=1:numData
            lambda2(j,1)=lambda(j,1)*c(j,1)*Kernel(i,j);
        end
        
        lambda(i,1)= lambda(i,1)+(1/Kernel(i,i))*(1-(c(i,1)*sum(lambda2)));
    
        % set a bound on lambda to prevent divergence
        if lambda(i,1)<0
            lambda(i,1)=0;
        elseif lambda(i,1)>lambdaBound
            lambda(i,1)=lambdaBound; 
        end
        
    end
    
    w2=(lambda.*c).*X;
    normW=norm(w2-w1);
end

lamPos = lambda > 0;
X_lamPos=X(lamPos,:); 
Y_lamPos=c(lamPos>0);

W=(lambda(lamPos).*Y_lamPos)'*X_lamPos;

b=mean(Y_lamPos-(X_lamPos*W'));  

ft=X*W'+b;


xPoints = [-50 50];
boundary = (-b-W(1)*xPoints)/W(2);
axis manual
plot(xPoints,boundary,'LineWidth',2,'Color','g');
legend('data1','data2','svm boundary');
title('SVM Boundary')



