% Generate Datas
data1 = randn(250, 2);
data2 = randn(250, 2) + [5 5];

% Collect Data into one Matrix
data = [data1; data2];

% Generate class vector
c = ones(size(data, 1), 1);
c(size(data2, 1)+1:end) = -1;