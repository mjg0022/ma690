% project 1 part 1 PCA
close all; clear all; clc;

m = 3; %slope
b = 0; %y-intercept

x = (-10:1:10)';

y = m*x + b;
y = y - mean(y); %shouldn't matter if b=0

figure; hold on; grid on;
plot(x,y);


X = [x y];

%[coeff] = pca(X);


xCov = cov(x,y);

[V,D] = eig(xCov) %eigenvectors and eigenvalues


newdata = V * X';
newdata = newdata';
newdata = fliplr(newdata);


var(newdata);


plot(x,newdata(:,1))

keyboard








% [coeff, score, roots] = pca(X);
% %basis = coeff(:,1);
% 
% pcSpace = X*coeff;


% keyboard
% 
% [coeff,score,latent] = pca([x y]);
% 
% 
% covMat = cov(x,y);
% [V,D] = eig(covMat);
% 
% 
% pcData = [x y]*coeff;
% corrcoef(pcData)