clear, clc, close all

rng(0)
data(:,1) = randn(30,1);
data(:,2) = 1.9 * data(:,1);
%data(:,2) = data(:,2) + 0.2*randn(size(data(:,1)));
data = sortrows(data,1);

figure
axes('LineWidth',0.6,...
    'FontName','Helvetica',...
    'FontSize',8,...
    'XAxisLocation','Origin',...
    'YAxisLocation','Origin');
line(data(:,1),data(:,2),...
    'LineStyle','None',...
    'Marker','o');
axis equal


%step 1
data(:,1) = data(:,1)-mean(data(:,1));
data(:,2) = data(:,2)-mean(data(:,2));


%step 2
C = cov(data)


%step 3
[V,D] = eig(C)



figure
axes('LineWidth',0.6,...
    'FontName','Helvetica',...
    'FontSize',8,...
    'XAxisLocation','Origin',...
    'YAxisLocation','Origin');
line(data(:,1),data(:,2),...
    'LineStyle','None',...
    'Marker','o');
line([0 V(1,1)],[0 V(2,1)],...
    'Color',[0.8 0.5 0.3],...
    'LineWidth',0.75);
line([0 V(1,2)],[0 V(2,2)],...
    'Color',[0.8 0.5 0.3],...
    'LineWidth',0.75);
axis equal


norm(V(:,1))
norm(V(:,2))
dot(V(:,1),V(:,2))


%step 4
newdata = V * data';
newdata = newdata';
newdata = fliplr(newdata)























