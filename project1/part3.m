%data = [1 2 3; 4 5 6; 7 8 9; 10 11 12];

t = linspace(0,pi,100);

sig1 = exp(1i*5*t);
sig2 = exp(1i*10*t);

figure; hold on;
plot(t,sig1);
plot(t,sig2);

% numobs = 3, numSig = 2
A = randn(3,2);

dataMat = [sig1 ; sig2];

mixedDataMat = A*dataMat;




%A = randn(size(data,2),size(data,1));
%mixedData = A*data;

%mixedData = transpose(data*A);