%MA690 Homework 2 Problem 3
% Run the generateSurface function
outStruct = generateSurface;

% Unpack the results
x = outStruct.x;
y = outStruct.y;
z = outStruct.z;

co = outStruct.co; % These are the true coefficients used.


% Your code goes here
%construct A
percOfPoints = 100;
numPoints = round(length(x)*(percOfPoints/100));
x = x(1:numPoints);
y = y(1:numPoints);
z = z(1:numPoints);

A = [x.^2 y.^2 x.*y x y ones(size(x))];

%b is the z values of the surface
b = z;

%Ax = b -> x = b/A -> A\b
abcdef = A\b;


% analysis
T = table(co,abcdef, ...
    'VariableNames',{'true' 'calc'}, ...
    'RowNames', {'a';'b';'c';'d';'e';'f'});

disp(T);

