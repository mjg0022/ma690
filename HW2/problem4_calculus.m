%MA690 homework 2 problem 4
%find the gradient of the matyas function
%for use in gradient descent
syms x y

f = 0.26*(x^2 + y^2) - 0.48*x*y;

gradient(f,[x,y])
