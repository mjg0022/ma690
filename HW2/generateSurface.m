function outStruct = generateSurface()

    [x, y] = ndgrid(randn(25), randn(25));
    
    co = randn(6, 1);
        
    z = co(1) .* x(:).^2 + co(2) .* y(:).^2 + co(3) .* x(:) .* y(:) + co(4) * x(:) + co(5) * y(:) + co(6);
    
    outStruct    = [];
    outStruct.x  = x(:);
    outStruct.y  = y(:);
    outStruct.z  = z;
    outStruct.co = co;    

end