% Run the generateSurface function
outStruct = generateSurface;

% Unpack the results
x = outStruct.x;
y = outStruct.y;
z = outStruct.z;

co = outStruct.co; % These are the true coefficients used.

%% Your code goes here
