%MA690 Homework 2 Problem 4
%Gradient Descent on Matyas Function
close all; clear all;

initialPoint = [-3,0]; %starting point for gradient descent
stepSize = .5; %step size for gradient descent a_n+1 = a - stepsize*grad(f)

%matyas function
f = @(x,y) 0.26*(x^2 + y^2) - 0.48*x*y;

%matyas gradient
fGradX = @(x,y) (13*x)/25 - (12*y)/25;
fGradY = @(x,y) (13*y)/25 - (12*x)/25;

%plot matyas
[xGrid, yGrid] = ndgrid(linspace(-5,5,51));
values = arrayfun(f,xGrid, yGrid);
f1 = figure; hold on; grid on;
imagesc(xGrid(:,1),yGrid(1,:),values);
scatter(0,0,'g.');
scatter(initialPoint(1),initialPoint(2),'m.');
axis([min(xGrid(:,1)) max(xGrid(:,1)) min(yGrid(1,:)) max(yGrid(1,:))])
set(gca,'YDir','normal');

title({['initialPoint: ' mat2str(initialPoint)] ['stepSize: ' num2str(stepSize)]});
xlabel('x'); ylabel('y');
colorbar;

% %plot gradient of matyas
% [xGridLowRes, yGridLowRes] = ndgrid(linspace(-5,5,11));
% G1 = arrayfun(fGradX,xGridLowRes,yGridLowRes);
% G2 = arrayfun(fGradY,xGridLowRes,yGridLowRes);
% f2 = figure;
% quiver(xGridLowRes(:,1),yGridLowRes(1,:),G1,G2)
% title('matyas gradient'); xlabel('x'); ylabel('y');
% axis([min(xGrid(:,1)) max(xGrid(:,1)) min(yGrid(1,:)) max(yGrid(1,:))])

iter = 0;
a = initialPoint;
figure(f1);

while 1 < 2
    iter = iter + 1;
    %display(['iter: ' num2str(iter)])
    
    %gradient descent equation
    a(end+1,:) = a(end,:) - stepSize*[fGradX(a(end,1),a(end,2)) fGradY(a(end,1),a(end,2))];
    
    %scatter(a(end,1),a(end,2),'m.')
    
    %M(iter) = getframe;
    
    if iter > 1000 || ( norm([a(end,:) - a(end-1,:)]) < .00001 )
       break 
    end
    
    %pause(.05)
end


display(['iterations: ' num2str(iter)])
display(['final point: ' mat2str(a(end,:),5)])
comet(a(:,1),a(:,2))

















