clear all; close all;

numLookSigs = 3;

load('project1Data.mat');
mixedDataMat = obs(1:10,:);

% center the data
%mixedDataMat = mixedDataMat - mean(mixedDataMat,'all');

%center
mu = mean(mixedDataMat,2);
mixedDataMat = bsxfun(@minus,mixedDataMat,mu);

% whiten the data
mixedDataMat = transpose(whiten(transpose(mixedDataMat)));



%center
% mu = mean(mixedDataMat,2);
% mixedDataMat = bsxfun(@minus,mixedDataMat,mu);

%whiten
% R = cov(transpose(mixedDataMat));
% [U, S, ~] = svd(R,'econ');
% T  = U * diag(1 ./ sqrt(diag(S))) * U.';
% mixedDataMat = T * mixedDataMat;


figure; hold on;
% plot(t,mixedDataMat(1,:));
% plot(t,mixedDataMat(2,:));
%plot(t,mixedDataMat(3,:));


%step 2
C = cov(transpose(mixedDataMat));

%step 3
[V,D] = eig(C)

%eigVals = D(numLookSigs:end,numLookSigs:end);

gDot = @(x) .5 * x.^2;
gDotDot = @(x) x;

z = mixedDataMat;

W = eye(size(D)); %eye(numLookSigs);

iter = 0;

while 1 < 2
    
    iter = iter + 1;
    
    Wprev = W;
    
    eta = transpose(Wprev)*z;
    %eta = ctranspose(Wprev)*z;
    etaAbsSqr = abs(eta).^2;
    
%     W = mean(gDot(etaAbsSqr).'*eta*z.','all')/numObs ...
%          - mean( gDotDot(etaAbsSqr)*etaAbsSqr + gDot(etaAbsSqr).' ) * Wprev;
    
    W = mean( gDot(etaAbsSqr) .* eta .* z , 2) / 10000 ...
        - bsxfun(@times, mean( gDotDot(etaAbsSqr).*etaAbsSqr + gDot(etaAbsSqr), 2) / 10000, Wprev);
    
    %normalize
    W = W ./ vecnorm(W,2,2);
    
    %decorrelate
    [U, S, ~] = svd(W,'econ');
    %[U, S, ~] = eig(W);
    W = U * diag(1 ./ diag(S)) * U.' * W;
    
    if iter > 10000 || max(1 - abs(dot(W,Wprev,2))) < eps
        break
    end
    
    display([num2str(iter) '   ' num2str(max(1 - abs(dot(W,Wprev,2))))]);

    
end

zICA = W*z;

%figure; hold on;
% plot(t,zICA(1,:));
% plot(t,zICA(2,:));
%plot(t,zICA(3,:));


%corrcoef(zICA(1,:),z(1,:))
%corrcoef(zICA(2,:),z(2,:))
%corrcoef(zICA(3,:),z(3,:))


%step 4
% unmixedData = V * mixedDataMat;
% unmixedData = unmixedData;
% unmixedData = fliplr(unmixedData)

% figure
% plot(unmixedData(1,:))

%A = randn(size(data,2),size(data,1));
%mixedData = A*data;

%mixedData = transpose(data*A);





































