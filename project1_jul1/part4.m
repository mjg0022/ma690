clear all; close all;

numLookSigs = 1;
% 
% t = linspace(0,2*pi,1000);
% 
% sig1 = cos(5*t); %exp(1i*5*t);
% sig2 = cos(15*t); %exp(1i*15*t);
% %sig2 = sawtooth(t*10);
% 
% figure; hold on;
% plot(sig1);
% plot(sig2);
% 
% % numobs = 3, numSig = 2
% numObs = 3;
% numSig = 2;
% A = randn(numObs,numSig);



% dataMat = [sig1 ; sig2];
% 
% mixedDataMat = A*dataMat;

load('project1Data.mat');
mixedDataMat = obs(1:10,:);

% figure; hold on;
% plot(mixedDataMat(1,:));
% plot(mixedDataMat(2,:));

% center the data
%mixedDataMat = mixedDataMat - mean(mixedDataMat,'all');

%center
mu = mean(mixedDataMat,2);
mixedDataMat = bsxfun(@minus,mixedDataMat,mu);

% whiten the data
%mixedDataMat = transpose(whiten(transpose(mixedDataMat)));

%whiten
R = cov(transpose(mixedDataMat));
[U, S, ~] = svd(R,'econ');
T  = U * diag(1 ./ sqrt(diag(S))) * U.';
mixedDataMat = T * mixedDataMat;




% figure; hold on;
% plot(mixedDataMat(1,:));
% plot(mixedDataMat(2,:));


%step 2
C = cov(transpose(mixedDataMat));

%step 3
[V,D] = eig(C)

eigVals = D(numLookSigs:end,numLookSigs:end);

gDot = @(x) .5 * x.^2;
gDotDot = @(x) x;

z = mixedDataMat;

W = eye(size(z,1)); %eye(numLookSigs);

iter = 0;

while 1 < 2
    
    iter = iter + 1;
    
    Wprev = W;
    
    eta = ctranspose(Wprev)*z;
    etaAbsSqr = abs(eta).^2;
    
%     W = mean(gDot(etaAbsSqr).'*eta*z.','all')/numObs ...
%          - mean( gDotDot(etaAbsSqr)*etaAbsSqr + gDot(etaAbsSqr).' ) * Wprev;
    
    W = mean( gDot(etaAbsSqr) .* eta .* z , 2) ...
        - bsxfun(@times, mean( gDotDot(etaAbsSqr).*etaAbsSqr + gDot(etaAbsSqr), 2), Wprev);
    
    %normalize
    W = W ./ vecnorm(W,2,2);
    
    %decorrelate
    %[U, S, ~] = svd(W,'econ');
    [U, S, ~] = eig(W);
    W = U * diag(1 ./ diag(S)) * U.' * W;
    
    if iter > 100
        break
    end
    
    display([num2str(iter) '   ' num2str(sum(W-Wprev,'all'))]);

    
end

zICA = W*z;

% figure; hold on;
% plot(zICA(1,:));
% plot(zICA(2,:));

%step 4
% unmixedData = V * mixedDataMat;
% unmixedData = unmixedData;
% unmixedData = fliplr(unmixedData)

% figure
% plot(unmixedData(1,:))

%A = randn(size(data,2),size(data,1));
%mixedData = A*data;

%mixedData = transpose(data*A);





































