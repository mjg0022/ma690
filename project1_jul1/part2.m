%project 1 part 2 pca on 2d nornal distribution
clear all; close all;

numDataPoints = 10000;

A = randn(2,2);

data = A*(randn(2,numDataPoints));

figure; hold on; grid on;
scatter(data(1,:),data(2,:),'.');
axis equal;