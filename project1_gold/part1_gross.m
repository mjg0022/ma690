% project 1 part 1 PCA
% Michael Gross
close all; clear all; clc;

m = 2; %slope

x = (-5:1:5)';

% this works pretty well for scaling the length of the plotted eigenvectors
scale = m*3;

y = m*x + 0;
y = y - mean(y); %data should already be centered

figure; hold on; grid on;
plot(x,y,'go');

X = [x y];

%pca
C = cov(X);
[V,D] = eig(C); %eigenvectors and eigenvalues

eig1st = [V(1,2) V(2,2)]; %lesser components are top left
eig2nd = [V(1,1) V(2,1)]; %principal component is bottom right

quiver([0 0],[0 0],[eig1st(1) 0],[eig1st(2) 0], ...
    'AutoScaleFactor',scale, ...
    'LineWidth', 2, ...
    'Color', 'r');
quiver([0 0],[0 0],[eig2nd(1) 0],[eig2nd(2) 0], ...
    'AutoScaleFactor',scale, ...
    'LineWidth', 2, ...
    'Color', 'b');


legend('data','primary eig','secondary eig')

axis equal








