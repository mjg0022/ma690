% project 1 part 4 ICA to unmix songs
% Michael Gross
clear all; close all;

numSig = 6;

load('project1Data.mat');
mixedDataMat = obs(1:10,:);

%% Pre-processing for ICA
%center the mixed data
mu = mean(mixedDataMat,2);
mixedDataMat = bsxfun(@minus,mixedDataMat,mu);

%whiten the mixed data
[U,Lam] = eig(cov(transpose(mixedDataMat)));
U = U(:,end-numSig+1:end);
Lam = diag(Lam);
Lam = Lam(end+1-numSig : end);
whitenMat = diag(1 ./ sqrt(Lam)) * ctranspose(U);
mixedDataMat = whitenMat*mixedDataMat;

%% Perform ICA iterations
gDot = @(x) .5 * (x.^2);
gDotDot = @(x) x;

z = mixedDataMat;
W = eye(numSig); %eye(numLookSigs);

iter = 0;

while 1 < 2
    
    iter = iter + 1;
    
    Wprev = W;
    
    eta = ctranspose(Wprev)*z;
    %eta = ctranspose(Wprev)*z;
    etaAbsSqr = abs(eta).^2;
    
    %good
    E1 = transpose(((gDot(etaAbsSqr) .* conj(eta)) * transpose(z)) / size(z,2));
    E2 = diag(mean(gDotDot(etaAbsSqr).*etaAbsSqr + gDot(etaAbsSqr),2))*W;    
    
    W = E1 - E2;
    
    %normalize
    % norm of matrix seems to work better
    %vecnorm(W,2,2);
    W = W ./ norm(W); 
    
    %decorrelate
    W = W*( (ctranspose(W)*W)^(-1/2) );
    
    
    display([num2str(iter) '   ' num2str(max(1 - abs(dot(W,Wprev,2))))]);
    
    if iter > 1000 || max(1 - abs(dot(W,Wprev,2))) < eps
        break
    end
    
end

zUnmixed = ctranspose(W) * z;


%% use this code block to play all of the isolated signals
choice = input(['Press Enter to play the isolated sound clips (Ctrl-C to cancel):' ]);

lengthToPlay = 10; %seconds
lengthToPlay = min(lengthToPlay,length(zUnmixed)/fs); %limit to max signal length

for i = 1:numSig
    display(['playing signal # ' num2str(i)]);
    soundsc(zUnmixed(i,:),fs);
    pause(lengthToPlay)
    clear sound;

end










