%project 1 part 2 pca on 2d nornal distribution
%Michael Gross
clear all; close all;

numDataPoints = 1000;

A = randn(2,2);

data = A*(randn(2,numDataPoints));

figure; hold on; grid on;
scatter(data(1,:),data(2,:),'g.');
axis equal;

X = data;

[U,Lam] = eig(cov(transpose(X)));
U = U(end-1:end,end-1:end);
Lam = diag(Lam);
Lam = Lam(end+1-2 : end);
V = U;

eig1st = [V(1,2) V(2,2)]; %lesser components are top left
eig2nd = [V(1,1) V(2,1)]; %principal component is bottom right

quiver([0 0],[0 0],[eig1st(1) 0],[eig1st(2) 0], ...
    'AutoScaleFactor',1, ...
    'LineWidth', 2, ...
    'Color', 'r');
quiver([0 0],[0 0],[eig2nd(1) 0],[eig2nd(2) 0], ...
    'AutoScaleFactor',1, ...
    'LineWidth', 2, ...
    'Color', 'b');


axis equal

legend('data','primary eig','secondary eig')









