% project 1 part 3 ICA implementation
% Michael Gross
clear all; close all;

numSig = 3; % number of signals to search for
numObs = 7; % number of mixed observations to generate

t = linspace(0,10*pi,100000);

sigs = [ exp(1i*1*t); ...
         exp(1i*5*t);
         exp(1i*.2*t)]; ...

% sigs = [ cos(1*1*t); ...
%          sin(1*5*t);
%          cos(1*.2*t)]; ...
     
%sig1 = exp(1i*1*t);%cos(t); %+ .1*randn(size(t)); %exp(1i*5*t); %cos(5*t); 
%sig2 = exp(1i*5*t);%sin(4*t); %+ .1*randn(size(t)); %sin(6*t); %exp(1i*15*t); %cos(15*t); 
         %exp(1i*10*t)];
%sig3 = cos(24*t);
%sig2 = sawtooth(t*10);

figure;
subplot(4,1,1); hold on;
for i = 1:size(sigs,1)
    plot(t,sigs(i,:));
end
title('original signals');

% initial mixing matrix
A = randn(numObs,numSig);

% apply the mixing matrix to the data
mixedDataMat = A*sigs;

%figure; hold on;
subplot(4,1,2); hold on;
for i = 1:numObs
    plot(t,mixedDataMat(i,:));
end
title('mixed signal observations');

%% Pre-processing for ICA
%center the mixed data
mu = mean(mixedDataMat,2);
mixedDataMat = bsxfun(@minus,mixedDataMat,mu);

%whiten the mixed data
[U,Lam] = eig(cov(transpose(mixedDataMat)));
U = U(:,end-numSig+1:end);
Lam = diag(Lam);
Lam = Lam(end+1-numSig : end);
whitenMat = diag(1 ./ sqrt(Lam)) * ctranspose(U);
mixedDataMat = whitenMat*mixedDataMat;

subplot(4,1,3); hold on;
for i = 1:size(mixedDataMat,1)
    plot(t,mixedDataMat(i,:));
end
title('mixed after whitening');

%% Perform ICA iterations
gDot = @(x) .5 * (x.^2);
gDotDot = @(x) x;

z = mixedDataMat;
W = eye(numSig); %eye(numLookSigs);

iter = 0;

while 1 < 2
    
    iter = iter + 1;
    
    Wprev = W;
    
    eta = ctranspose(Wprev)*z;
    %eta = ctranspose(Wprev)*z;
    etaAbsSqr = abs(eta).^2;
    
    %good
    E1 = transpose(((gDot(etaAbsSqr) .* conj(eta)) * transpose(z)) / size(z,2));
    E2 = diag(mean(gDotDot(etaAbsSqr).*etaAbsSqr + gDot(etaAbsSqr),2))*W;    
    
    W = E1 - E2;
    
    %normalize
    W = W ./ vecnorm(W,2,2);
    
    %decorrelate
    W = W*( (ctranspose(W)*W) ^ (-1/2));
    
    if iter > 1000 || max(1 - abs(dot(W,Wprev,2))) < eps
        break
    end
    
    display([num2str(iter) '   ' num2str(max(1 - abs(dot(W,Wprev,2))))]);

    
end

zUnmixed = W'*z;

%figure; hold on;
subplot(4,1,4); hold on;
for i = 1:size(zUnmixed,1)
   plot(t,zUnmixed(i,:)) 
end
title('unmixed signals');


