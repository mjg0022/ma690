%MA690 homework 2 problem 4
%find the gradient
syms x y

f = sin(3*pi*x)^2 + ((x-1)^2 + (y-1)^2)*(1+sin(3*pi*y)^2);

gradient(f,[x,y])
