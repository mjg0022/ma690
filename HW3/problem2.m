close all; clear all;

partChoice = 4;
%1 is part 1
%2 is part 2
%3 is part 3.1
%4 is part 3.2

for partChoice = 1:4

switch partChoice
    case 1
        initialPoint = [1.2, 1.05];
        stepSize = .01;
        B = 0;
    case 2
        initialPoint = [3,3];
        stepSize = .05;
        B = .001;
    case 3
        initialPoint = [1.16, 1.05];
        stepSize = .01;
        B = 0;
    case 4
        initialPoint = [1.16, 1.05];
        stepSize = .01;
        B = .75;
    otherwise
        error('invalid choice')
end

%function
%f = @(x,y) sin(3*pi*x)^2 + ((x-1)^2 + (y-1)^2)*(1+sin(3*pi*y)^2);
%sunkes function
f = @(x,y) sin(3*pi*x).^2 + ((x - 1).^2 + (y - 1).^2) .* (1 + sin(3 * pi * y).^2);

%gradient
%fGradX = @(x,y) (2*x - 2)*(sin(3*pi*y)^2 + 1) + 6*pi*cos(3*pi*x)*sin(3*pi*x);
%fGradY = @(x,y) (2*y - 2)*(sin(3*pi*y)^2 + 1) + 6*pi*cos(3*pi*y)*sin(3*pi*y)*((x - 1)^2 + (y - 1)^2);
%sunkes gradient
fGradX = @(x,y) 2*sin(3*pi*x)*cos(3*pi*x)*3*pi + 2*(x - 1) * (1 + sin(3 * pi * y).^2);
fGradY = @(x,y) 2 * (y - 1) .* (1 + sin(3 * pi * y).^2) + 2 * ((x - 1).^2 + (y - 1).^2) .* sin(3 * pi * y) .* cos(3 * pi * y) * 3 * pi;

%plot
[xGrid, yGrid] = ndgrid(linspace(-5,5,51));
values = arrayfun(f,xGrid, yGrid);
f1 = figure; hold on; grid on;
imagesc(xGrid(:,1),yGrid(1,:),values);
scatter(1,1,'g.');
scatter(initialPoint(1),initialPoint(2),'m.');
axis([min(xGrid(:,1)) max(xGrid(:,1)) min(yGrid(1,:)) max(yGrid(1,:))])
set(gca,'YDir','normal');

title({['initialPoint: ' mat2str(initialPoint)] ... 
        ['alpha: ' num2str(stepSize) '   |   Beta: ' num2str(B)]});
xlabel('x'); ylabel('y');
colorbar;

% %plot gradient
% [xGridLowRes, yGridLowRes] = ndgrid(linspace(-5,5,11));
% G1 = arrayfun(fGradX,xGridLowRes,yGridLowRes);
% G2 = arrayfun(fGradY,xGridLowRes,yGridLowRes);
% f2 = figure;
% quiver(xGridLowRes(:,1),yGridLowRes(1,:),G1,G2)
% title('gradient'); xlabel('x'); ylabel('y');
% axis([min(xGrid(:,1)) max(xGrid(:,1)) min(yGrid(1,:)) max(yGrid(1,:))])

iter = 0;
a = initialPoint;
figure(f1);

while 1 < 2
    iter = iter + 1;
    %display(['iter: ' num2str(iter)])
    
    %gradient descent equation
    if iter == 1
        a(end+1,:) = a(end,:) - stepSize*[fGradX(a(end,1),a(end,2)) fGradY(a(end,1),a(end,2))];
                            %+ B*(a(end,:) - a(end-1,:));
    else
        a(end+1,:) = a(end,:) - stepSize*[fGradX(a(end,1),a(end,2)) fGradY(a(end,1),a(end,2))] ...
                            + B*(a(end,:) - a(end-1,:));
    end
    

    scatter(a(end,1),a(end,2),'m.')

    
    %M(iter) = getframe;
    
    if iter > 999 || ( norm([a(end,:) - a(end-1,:)]) < 1e-9 )
       break 
    end
    
    %pause(.05)
end


display(['initialPoint: ' mat2str(initialPoint)])
display(['alpha: ' num2str(stepSize)])
display(['Beta: ' num2str(B)])
display(['iterations: ' num2str(iter)])
display(['final point: ' mat2str(a(end,:),5)])
display([char(10) char(10) char(10) char(10)])
%comet(a(:,1),a(:,2))

end















