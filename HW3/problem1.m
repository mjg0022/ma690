clc, clear all, format compact;

alpha = 0.001;
lambda = 2;
numIter = 1000;

% Load Data
truth = load("blocks.txt");  
b     = load("blocks_noisy.txt");

% Generate the matrix D
D = inv(triu(ones(numel(truth))));

x = b;
costVec = [];
x_history = x;

for iter = 1:numIter
   
   %subg = .5*norm(x - b,2)^2 + lambda*norm(D*x,1);
   subg = 2*(x-b) + lambda*transpose(D)*sign(D*x); 
   
   x = x - alpha*subg;
   
   x_history(:,end+1) = x;
   
   costVec(end+1) = norm(x - b,2)^2 + lambda*norm(D*x,1);
    
end

[minCost, minCostLoc] = min(costVec);

figure; hold on
plot(x_history(:,minCostLoc),'r');
plot(b,'b')
plot(truth,'g')
legend('denoised','noisy','truth')
